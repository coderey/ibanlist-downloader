<?php


namespace Coderey\IbanlistDownloader;



use GuzzleHttp\Client;

class BundesbankLoader
{
    const KEYWORD_CURRENT  = 'aktuell';
    const KEYWORD_UPCOMING = 'neu';

    public static function getCurrentIbanFile()
    {
        return self::getIbanFiles([self::KEYWORD_CURRENT])[0];
    }

    public static function getUpcomingIbanFile()
    {
        return self::getIbanFiles([self::KEYWORD_UPCOMING])[0];
    }

    public static function getIbanFiles($status = [self::KEYWORD_CURRENT, self::KEYWORD_UPCOMING])
    {
        $client   = new Client();
        $response = $client->get('https://www.bundesbank.de/de/aufgaben/unbarer-zahlungsverkehr/serviceangebot/bankleitzahlen/download-bankleitzahlen-602592');
        $html     = $response->getBody()->getContents();
        $regex    = '/\<a [^>]*href=\"([^"]*blz-(' . implode('|', $status) . ')-txt-data.txt)\"[^>]*\>(.*?)\<\/a\>/is';
        $links    = [];

        if (preg_match_all($regex, $html, $out)) {
            foreach ($out[1] as $k => $url) {
                $linktext      = strip_tags($out[3][$k]);
                $linktextRegex = '/Bankleitzahlendateien - gültig vom (?P<validFrom>[0-9\.]+) bis (?P<validTo>[0-9\.]+)/is';
                $datesFound    = preg_match($linktextRegex, $linktext, $dates);

                $links[] = [
                    'url'        => 'https://www.bundesbank.de/' . $url,
                    'status'     => $out[2][$k],
                    'valid_from' => $datesFound ? $dates['validFrom'] : '',
                    'valid_to'   => $datesFound ? $dates['validTo'] : '',
                ];
            }

        } else {
            throw new \Exception('Could not parse my download-links on given page');
        }

        return $links;
    }

}