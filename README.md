Iban-List Downloader
====================

This is a support-tool that could be used for updating openIBAN databases.


The german bank-numbers are officially defined here:
https://www.bundesbank.de/de/aufgaben/unbarer-zahlungsverkehr/serviceangebot/bankleitzahlen/download-bankleitzahlen-602592

but the download-links have hashs in it - and I don't want to go to this website every three month and download the current file manually...

so this small class should parse the website for download-links...

While they don't rename the filenames and don't change the whole website-structure, this script should get the current file - in next step you can use this information to build auto-update-mechanics in openIBAN for german bank-accounts or do any other magic with it...

here are links to the tool for which I want to use these files:

https://github.com/apilayer/goiban-service

https://hub.docker.com/r/fourcube/openiban/

but feel free to use it in any creative way :-)
